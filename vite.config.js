import { defineConfig } from "vite";
import preact from "@preact/preset-vite";

export default defineConfig(({mode}) => ({
    build: {
        outDir: "public",
        emptyOutDir: mode === "production",
    },
    publicDir: "static",
    plugins: [
        preact(),
    ],
}));
