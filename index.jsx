import { always, compose } from "@fpjs/overture/base";
import { equals } from "@fpjs/overture/algebras/setoid";
import { map } from "@fpjs/overture/algebras/functor";
import { over, set, Indexed, Record } from "@fpjs/overture/control/lens";
import { patchBuiltins } from "@fpjs/overture/patches";
import { App, Time } from "@fpjs/signal";
const { Eff, start, noEffects, transition } = App;

import { Component, h, render} from "preact";

const _ = Record;
const {ix} = Indexed;

const Cell = ({cell, onClick}) => (
    <span class={`cell ${cell.alive ? 'alive' : ''}`} onClick={onClick}></span>
);

const Row = ({row, cells, onClickCell}) => (
    <div class="row">
      { map (([cell, idx]) => <Cell cell={cell} onClick={() => onClickCell(row, idx)} />) (zipWithIndex(cells)) }
    </div>
);

const Grid = ({matrix, onClickCell}) => (
    <div class="matrix">
      { map (([cells, idx]) => <Row row={idx} cells={cells} onClickCell={onClickCell} />) (zipWithIndex(matrix)) }
    </div>
);

const Game = ({state, input}) => {
    const seeding = state.type === 'Seeding';
    const paused = state.type === 'Paused';
    const running = state.type === 'Running';
    const halted = state.type === 'Halted';
    return (
        <div class="main">
          <div class="controls">
            { (seeding || paused)  && <span class="control start" onClick={() => input(Start())}>Start</span> }
            { running && <span class="control pause" onClick={() => input(Pause())}>Pause</span> }
            { halted && <span class="control reset" onClick={() => input(Reset())}>Reset</span> }
            <span class="state">
              { seeding && 'Create the seed' }
              { paused && 'Paused' }
              { running && `Generation ${state.generation}` }
              { halted && `Finished after generation ${state.generation}` }
            </span>
          </div>
          <Grid matrix={state.matrix}
                onClickCell={(row, cell) => input(FlipCell(row, cell))}/>
        </div>
    );
};

const AboutPage = ({input}) => (
    <article>
      <header>
        <h1>Conway's Game of Life</h1>
      </header>
      <blockquote>
        <p>
          The game is a zero-player game, meaning that its evolution is determined
          by its initial state, requiring no further input. One interacts with the
          Game of Life by creating an initial configuration and observing how it
          evolves, or, for advanced players, by creating patterns with particular
          properties.
        </p>
        <footer>
          — <a href="https://en.wikipedia.org/wiki/Conway's_Game_of_Life">Wikipedia</a>
        </footer>
      </blockquote>
      <h2>Rules</h2>
      <ul>
        <li>Any live cell with fewer than two live neighbours dies, as if by underpopulation</li>
        <li>Any live cell with two or three live neighbours lives on to the next generation</li>
        <li> live cell with more than three live neighbours dies, as if by overpopulation</li>
        <li>Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction</li>
      </ul>
      <div class="controls">
        <span onClick={() => input(Play())} class="control play">Play</span>
      </div>
    </article>
);

const GoL = ({state, input}) => (
    <div class="page">
      <div class="content">
        { state.type === 'About' && <AboutPage input={input} /> }
        { state.type !== 'About' && <Game state={state} input={input} /> }
      </div>
      <footer>
        An implementation of <a href="https://en.wikipedia.org/wiki/Conway's_Game_of_Life">Conway's Game of Life</a>
        &nbsp;[<a href="https://gitlab.com/bjpbakker/goljs">source code</a>].
      </footer>
    </div>
);

const zipWithIndex = (xs) => xs.map((x, idx) => [x, idx]);

const FlipCell = (row, cell) => ({type: 'FlipCell', row, cell});
const Start = () => ({type: 'Start'});
const Pause = () => ({type: 'Pause'});
const Reset = () => ({type: 'Reset'});
const Play = () => ({type: 'Play'});

const mkcell = (alive) => ({
    alive,
    '@@type': 'gol/Cell',
    'fantasy-land/equals': (other) => other.alive === alive,
});

const gol = (matrix) => {
    const countNeighbors = (row,cell) => {
        let count = 0;
        for (let i = Math.max(row-1, 0); i <= Math.min(row + 1, 99); i++) {
            for (let j = Math.max(cell-1, 0); j <= Math.min(cell+1, 99); j++) {
                if (i === row && j === cell) {
                    continue; // thee are not thei neighbor
                }
                if (matrix[i][j].alive) {
                    count ++;
                }
            }
        }
        return count;
    };

    const copy = map ((xs) => xs.slice()) (matrix);
    for (let i = 0; i < matrix.length; i++) {
        for (let j = 0; j < matrix[i].length; j++) {
            const nc = countNeighbors(i,j);
            const cell = matrix[i][j];
            if (cell.alive && (nc < 2 || nc > 3)) {
                copy[i][j] = mkcell(false);
            }
            if (!cell.alive && nc === 3) {
                copy[i][j] = mkcell(true);
            }
        }
    }
    return copy;
}

const matchEvent = (matches) => (event) => {
    const match = matches[event.type];
    if (match === undefined) {
        return noEffects;
    }
    return match (event);
};

const flipCell = (x,y) => transition(
    over (compose (_.matrix) (compose (ix(x)) (ix(y)))) (({alive}) => mkcell(!alive))
);

const tick = (state) => {
    const evolved = gol (state.matrix);
    const halt = equals (state.matrix) (evolved);
    return noEffects({
        ...state,
        matrix: evolved,
        generation: state.generation + (halt ? 0 : 1),
        type: halt ? 'Halted' : state.type,
    });
};

const foldps = {
    'About': matchEvent({
        'Play': () => transition (set (_.type) ('Seeding')),
    }),
    'Seeding': matchEvent({
        'FlipCell': ({row, cell}) => flipCell(row, cell),
        'Start': () => transition (set (_.type) ('Running')),
    }),
    'Running': matchEvent({
        'Pause': () => transition (set (_.type) ('Paused')),
        'Tick': () => tick,
    }),
    'Paused': matchEvent({
        'Start': () => transition (set (_.type) ('Running')),
        'FlipCell': ({row, cell}) => flipCell(row, cell),
    }),
    'Halted': matchEvent({
        'Reset': () => transition (compose (set (_.type) ('Seeding')) (always (initState))),
    }),
};

const foldp = (event) => (state) => {
    const foldp_ = foldps [state.type];
    if (foldp_ === undefined) {
        throw TypeError(`No foldp for state ${state.type}`);
    }
    return foldp_ (event) (state);
};

const initState = {
    type: 'About',
    matrix: new Array(100).fill(new Array(100).fill(mkcell(false))),
    generation: 0,
};

const run = (root) => {
    const ticks = map (always({type: 'Tick'})) (Time.every(200));
    const config = {
        initState: initState,
        foldp: foldp,
        render: Eff(({state, input}) => render(<GoL state={state} input={input} />, root, root.lastElementChild)),
        inputs: [ticks]
    };

    const app = start (config);
};

const main = () => {
    const root = document.getElementById("app");
    if (root != null) {
        run(root);
    } else {
        console.log("Waiting for app root to render..");
        setTimeout(main, 10);
    }
};

patchBuiltins ();
main ();
